import warnings
import math
import os.path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from iminuit import Minuit
from scipy.optimize import curve_fit
import requests

from config import ECM, LUMI, XSEC, color, HDF_NAME, HDF_URL


def overlay(
    dfs,
    var,
    weights=None,
    cuts=None,
    ax=None,
    draw_legend=True,
    overlay_data=None,
    stacked=False,
    logy=False,
    **kwargs,
):
    """
    Create histograms from a dictionary of pandas DataFrames and overlay them

    Args:
        dfs (dict of :class:`pandas.DataFrame`): dictionary of pandas DataFrames
        var (str): The expression to evaluate for the histogram x-axis
        weights (str): The expression to evaluate for the weights.
        cuts (str or list(str)): Selection(s) to query before histogramming
        ax: The :class:`matplotlib.axes.Axes` to plot on. If not given the current axes is used.
        draw_legend: If True, draw a legend.
        overlay_data (:class:`pandas.DataFrame`): DataFrame containing data points to overlay.
        stacked: If True, stack the histograms
        logy: If True, plot the y-axis of each plot in logarithmic scale
        **kwargs: Additional keyword arguments will be passed to :meth:`matplotlib.axes.Axes.hist`

    Returns:
        Axes: The :class:`matplotlib.axes.Axes` used for the plot

    """
    keys = [i for i in dfs.keys()]
    cut = join_cuts(cuts)
    ax = ax or plt.gca()
    edges = kwargs.pop("bins", "auto")
    weight_expr = weights
    arrays = {}
    weights = {}
    total = []

    def get_array(df):
        if cut is not None:
            df = df.query(cut)
        return df.eval(var).to_numpy()

    def get_weights(df):
        if cut is not None:
            df = df.query(cut)
        return df.eval(weight_expr).to_numpy()

    for k, df in dfs.items():
        x = get_array(df)
        arrays[k] = x
        if weight_expr is not None:
            weights[k] = get_weights(df)
        total.append(x)

    if overlay_data is not None:
        x_data = get_array(overlay_data)
        total.append(x_data)

    total = np.concatenate(total)

    if (np.round(total) == total).all() and not kwargs.get("range", None):
        # in this case we have an integer quantity
        # and can adjust the binning appropriately
        edges = int(total.max() - total.min()) + 1
        kwargs.update(range=(total.min(), total.max() + 1))

    # this call is just there to define the binning
    _, edges = np.histogram(total, bins=edges, range=kwargs.pop("range", None))

    if stacked:
        hist_stack(
            list(arrays.values()),
            bins=edges,
            labels=list(arrays.keys()),
            keys=list(dfs.keys()),
            ax=ax,
            weights=list(weights.values()),
        )
        if not logy:
            ax.set_ylim(bottom=0)
    else:
        for k, x in arrays.items():
            ax.hist(
                x,
                bins=edges,
                label=k,
                linewidth=1.5,
                histtype="step",
                color=color[k],
                weights=weights[k] if weight_expr is not None else None,
                **kwargs,
            )

    def get_data_hist(x, edges):
        hist, edges = np.histogram(x, bins=edges, range=kwargs.pop("range", None))
        centers = edges[:-1] + 0.5 * (edges[1:] - edges[:-1])
        return centers, hist

    if overlay_data is not None:
        centers_data, hist_data = get_data_hist(x_data, edges)
        ax.errorbar(
            centers_data,
            hist_data,
            yerr=np.sqrt(hist_data),
            fmt=".",
            color="black",
            label="Data",
        )

    if draw_legend:
        handles, labels = ax.get_legend_handles_labels()
        if stacked:
            handles = reversed(handles)
            labels = reversed(labels)
        ax.legend(handles, labels)

    if logy:
        ax.set_yscale("log")

    ax.grid()

    return ax


def hist_stack(x, labels, keys, weights=None, ax=None, **kwargs):
    """
    Draw a stack of histograms, faster than :meth:`matplotlib.axes.Axes.hist` with ``stacked=True``

    Will order the histograms by integral (lowest at the bottom)
    """
    ax = ax or plt.gca()
    hists = []
    for i, x_i in enumerate(x):
        if weights:
            weights_i = weights[i]
        else:
            weights_i = None
        hist, edges = np.histogram(x_i, weights=weights_i, **kwargs)
        kwargs["bins"] = edges
        hist = np.append(hist, hist[-1])
        hists.append(hist)
    bottom = 0
    total = 0
    for i, (hist, label) in sorted(
        enumerate(zip(hists, labels)), key=lambda x: x[1][0].sum()
    ):
        total = total + hist
        ax.fill_between(
            edges, bottom, total, step="post", label=label, color=color[keys[i]]
        )
        bottom = total
    return ax


def join_cuts(cuts):
    if cuts is None:
        return None
    if isinstance(cuts, str):
        return cuts
    if len(cuts) == 0:
        return None
    return "(" + ") & (".join(cuts) + ")"


def overlay_many(
    dfs,
    exprs,
    weights=None,
    cuts=None,
    binnings=None,
    ncols=3,
    nrows=None,
    figsize=None,
    overlay_data=None,
    logy=False,
    **kwargs,
):
    """
    Run `overlay` for many expressions at once and arange the plots in a grid.

    Args:
        dfs (dict of :class:`pandas.DataFrame`): dictionary of pandas DataFrames
        exprs (list(str)): The expressions to evaluate for the x-axis of each plot.
        weights (str): The expression to evaluate for the weights.
        cuts (str or list(str)): Selection(s) to query before histogramming
            (same selection is applied for each plot)
        binnings (dict(dict)): dictionary of keyword arguments for :meth:`matplotlib.axes.Axes.hist`.
            Will be applied for keys matching the plotted quantity from ``exprs``.
        ncols (int): Number of columns in the grid of plots.
        nrows (int): Number of rows (if None will be automatically calculated).
        figsize (tuple(float)): Size of the figure (if None will be automatically calculated).
        overlay_data (:class:`pandas.DataFrame`): DataFrame for Data points to overlay.
        logy: If True, plot the y-axis of each plot in logarithmic scale
        **kwargs: Additional keyword arguments will be passed to :meth:`matplotlib.axes.Axes.hist`

    """
    if len(exprs) < ncols:
        ncols = len(exprs)
    if nrows is None:
        nrows = math.ceil(len(exprs) / ncols)
    if figsize is None:
        width = 16 / ncols
        height = 0.6 * width
        figsize = (ncols * width, nrows * height)
    if binnings is None:
        binnings = {}
    fig, axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, squeeze=False)
    ax_itr = iter(axs.ravel())
    for expr in exprs:
        try:
            ax = next(ax_itr)
        except StopIteration:
            warnings.warn(f"Not enough rows/columns defined, won't plot {expr}")
            continue
        expr_kwargs = dict(kwargs, bins=100)
        if expr in binnings:
            expr_kwargs.update(binnings[expr])
        overlay(
            dfs,
            expr,
            weights,
            cuts=cuts,
            ax=ax,
            draw_legend=False,
            overlay_data=overlay_data,
            logy=logy,
            **expr_kwargs,
        )
        ax.set_xlabel(expr)
    fig.legend(
        *ax.get_legend_handles_labels(),
        bbox_to_anchor=(0.05, 1.0, 0.9, 0.03),
        ncol=4 + int(overlay_data is not None),
        mode="expand",
    )
    # clear empty figures
    for ax in ax_itr:
        ax.set_axis_off()
    fig.tight_layout()


def join_df(df):
    dfmc = pd.DataFrame()
    for key in df.keys():
        dfin = df[key]
        dfmc = pd.concat([dfmc, dfin])
    dfmc.reset_index(inplace=True)
    return dfmc


def efficiencies_for(dfs, cuts):
    """
    Calculate the efficiencies for a certain selection.

    Args:
        dfs (dict of :class:`pandas.DataFrame): dictionary of pandas DataFrames.
        cuts (str or list(str)): Selection to calculate efficiencies for.

    Returns:
        dict(float): dictionary of efficiencies for each key in ``dfs``.

    """
    cut = join_cuts(cuts)
    N_tot = {}
    N_sel = {}
    for key, df in dfs.items():
        df_reduced = df
        if cut:
            df_reduced = df.query(cut)
        N_tot[key] = len(df)
        N_sel[key] = len(df_reduced)
    eff = {}
    for key in N_tot:
        eff[key] = N_sel[key] / N_tot[key]
    return eff


def tfitfunc(x, partfit, ecm, lumi, iflag=0):
    "Fit template for the s-, t-, s-t-interference channels for electrons"
    mz = 91.187
    partt = [0.10021, 0.09744, 0.09530, 0.09539, 0.09614, 0.09460, 0.09275]
    parst = [0.42600, 0.62501, 0.70557, -0.09992, -0.90750, -1.03696, -0.95935]
    ecmref = [88.400, 89.380, 90.230, 91.240, 92.070, 93.080, 93.910]
    lumi = (lumi / 1000.0) * ((1.8 / 100.0) / 0.02)

    if ecm < mz:
        afb = (ecm - mz) * 0.1
    else:
        afb = (ecm - mz) * 0.05
    funcvalue = np.zeros_like(x)
    if iflag == 0 or iflag == 1:
        # s-channel
        funcvalue += partfit * ((1 + x * x) + 2.0 * 4.0 / 3.0 * afb * x)
    if iflag == 0 or iflag == 2:
        # t-channel
        funcvalue += (
            lumi
            * 2
            * np.interp(ecm, ecmref, partt)
            * (4.0 + (1 + x) * (1 + x))
            / ((1 - x) * (1 - x))
        )
    if iflag == 0 or iflag == 3:
        # s-t-interference
        funcvalue += lumi * np.interp(ecm, ecmref, parst) * (1 + x) * (1 + x) / (1 - x)
    return funcvalue


def tfit_nll(centers, h_data, h_data_err, ecm, lumi):
    """
    Negative log likelihood to minimize for the fit
    Note: ignores the data errors (assumes poisson probability for the template)
    """

    def cost(partfit):
        # negative log likelihood
        f = tfitfunc(centers, partfit, ecm=ecm, lumi=lumi)
        y = h_data
        # formula taken from explanation in ROOT TH1::Fit
        # https://root.cern/doc/master/classTH1.html#a7e7d34c91d5ebab4fc9bba3ca47dabdd
        # twice this value can be interpreted like chi2
        epsilon = 1e-232
        return np.sum(f + y * np.log(y / f + epsilon) - y)

    m = Minuit(cost, partfit=1)
    m.errordef = Minuit.LIKELIHOOD
    m.migrad()
    m.hesse()

    param = m.values[0]
    error = m.errors[0]
    debug = m
    return param, error, debug


def tfit_and_plot(df_mc, df_data, ecm_name, cuts, ax=None):
    """
    Perform a fit of the s-channel contribution for electrons and plot the result.

    Args:
        df_mc (:class:`pandas.DataFrame`): Electron MC events.
        df_data (:class:`pandas.DataFrame`): Data events.
        ecm_name: key to lookup the correct cross section and luminosity, e.g. "ecm4"
        cuts: (str or list(str)): Selection to apply.
        ax: The :class:`matplotlib.axes.Axes` to plot on. If not given the current axes is used.

    Returns:
        tuple(float, float, :class:`iminuit.Minuit`): The number of fitted s-channel events,
            the uncertainty and a an :class:`iminuit.Minuit` object containing information on the fit
    """

    def histogram(df):
        return np.histogram(df.eval("Cos_thru").to_numpy(), range=(-0.9, 0.9), bins=100)

    h_tot, edges = histogram(df_mc)
    h_sel, edges = histogram(df_mc.query(join_cuts(cuts)))
    centers = edges[:-1] + 0.5 * (edges[1:] - edges[:-1])
    corr = h_tot / h_sel
    h_data, edges = histogram(df_data.query(join_cuts(cuts)))
    h_data_err_rel = 1.0 / np.sqrt(h_data)
    h_data = h_data * corr
    h_data_err = h_data_err_rel * h_data

    i = int(ecm_name.replace("ecm", "")) - 1
    lumi = LUMI[i]
    ecm = ECM[i]

    param, error, debug = tfit_nll(centers, h_data, h_data_err, ecm, lumi)

    ax = ax or plt.gca()
    ax.errorbar(centers, h_data, h_data_err, fmt=".", color="black", label="Data")
    ax.plot(
        centers,
        tfitfunc(centers, param, ecm=ecm, lumi=lumi),
        color="red",
        linewidth=2,
        label="combined",
    )
    ax.plot(
        centers,
        tfitfunc(centers, param, ecm=ecm, lumi=lumi, iflag=1),
        label="s-channel",
        linewidth=2,
    )
    ax.plot(
        centers,
        tfitfunc(centers, param, ecm=ecm, lumi=lumi, iflag=2),
        label="t-channel",
        linestyle="--",
    )
    ax.plot(
        centers,
        tfitfunc(centers, param, ecm=ecm, lumi=lumi, iflag=3),
        label="s-t-interference",
        linestyle="--",
    )
    ax.set_xlabel("cos(Thrust)")

    # normalization factor for selection efficiency
    # integral(-0.9, 0.9) = 2.286
    # integral(-1, 1) = 8 / 3
    area = 2.286
    norft = 8 / 3 / area

    n_schan = norft * tfitfunc(centers, param, ecm=ecm, lumi=lumi, iflag=1).sum()
    dn_schan = n_schan * error / param

    return n_schan, dn_schan, debug


# fmt: off
def breit_wigner(
    x,
    *par,
    piflav="lep",
    qed_corrections=True,
    s_dependent=True,
    gamma_exchange=True,
    gamma_z_interference=True
):
    xd = x
    mzpar = par[0]
    gzpar = par[1]
    peakpar = par[2]

    aqed = 1.0 / 128.9
    sw = 0.231
    gf = 1.16639e-5
    conv = 389387.0
    qegve = (-1.0) * (2 * sw - 0.5)
    mzlit = 91.185
    gzlit = 2.492

    if piflav == "had":
        qsum = 11.0 / 9.0
        ncolor = 3
        qfgvf = 2.0 * 2.0 / 3.0 * (0.5 - 2 * sw * 2.0 / 3.0) + 3.0 * (-1.0 / 3.0) * (
            (-0.5) - 2.0 * sw * (-1.0 / 3.0)
        )
    else:
        qsum = 1
        ncolor = 1
        qfgvf = qegve

    x2mz2 = (xd + mzpar) * (xd - mzpar)
    mzpar2 = mzpar * mzpar
    gzpar2 = gzpar * gzpar
    mzlit2 = mzlit * mzlit
    gzlit2 = gzlit * gzlit
    xd2 = xd * xd

    ergebnis = 0
    if s_dependent:
        ergebnis += (
            peakpar * xd2 * gzpar2 / (x2mz2 * x2mz2 + xd2 * xd2 / mzpar2 * gzpar2)
        )
    else:
        ergebnis += peakpar * xd2 * gzpar2 / (x2mz2 * x2mz2 + mzpar2 * gzpar2)
    if gamma_exchange:
        ergebnis += conv * 4.0 / 3.0 * np.pi * qsum * ncolor * aqed * aqed / xd2
    if gamma_z_interference:
        ergebnis += (
            conv * ncolor * np.sqrt(2.0) * 2.0 / 3.0 * aqed * gf * mzlit2 * qegve * qfgvf * x2mz2
            / (x2mz2 * x2mz2 + mzpar2 * gzpar2)
        )
    if qed_corrections:
        ergebnis /= qedcor(xd - mzpar, piflav=piflav)

    return ergebnis
# fmt: on


def qedcor(x, piflav="lep"):
    # fmt: off
    decm = np.array([
        -5.18840,-4.98840,-4.78840,-4.58840,-4.38840,-4.18840,-3.98840,-3.78840,-3.58840,-3.38840,
        -3.18840,-2.98840,-2.78840,-2.58840,-2.38840,-2.18840,-1.98840,-1.78840,-1.58840,-1.38840,
        -1.18840,-0.98840,-0.78840,-0.58840,-0.38840,-0.18840,0.01160,0.21160,0.41160,0.61160,
        0.81160,1.01160,1.21160,1.41160,1.61160,1.81160,2.01160,2.21160,2.41160,2.61160,2.81160,
        3.01160,3.21160,3.41160,3.61160,3.81160,4.01160,4.21160,4.41160,4.61160,4.81160,
        5.01160,5.21160,5.41160,5.61160,5.81160,6.01160,6.21160,6.41160,6.61160,
    ])
    rxshm = np.array([
        1.26770,1.27423,1.28087,1.28763,1.29453,1.30158,1.30880,1.31621,1.32379,1.33156,1.33954,
        1.34773,1.35613,1.36458,1.37335,1.38224,1.39114,1.39999,1.40836,1.41596,1.42220,1.42619,
        1.42668,1.42210,1.41062,1.39061,1.36132,1.32329,1.27842,1.22931,1.17853,1.12811,1.07941,
        1.03322,0.98988,0.94951,0.91204,0.87731,0.84515,0.81534,0.78769,0.76201,0.73812,0.71587,
        0.69510,0.67568,0.65751,0.64047,0.62425,0.60920,0.59520,0.58182,0.56918,0.55722,0.54589,
        0.53515,0.52496,0.51527,0.50606,0.49729
    ])
    rxsmu = np.array([
        1.13780,1.15097,1.16428,1.17771,1.19126,1.20491,1.21866,1.23249,1.24639,1.26035,1.27436,
        1.28840,1.30243,1.31643,1.33034,1.34407,1.35752,1.37051,1.38275,1.39384,1.40316,1.40980,
        1.41252,1.40971,1.39954,1.38043,1.35165,1.31381,1.26889,1.21958,1.16851,1.11778,1.06877,
        1.02232,0.97879,0.93829,0.90076,0.86607,0.83401,0.80439,0.77700,0.75164,0.72814,0.70633,
        0.68607,0.66720,0.64962,0.63321,0.61787,0.60352,0.59007,0.57745,0.56560,0.55445,0.54396,
        0.53408,0.52475,0.51595,0.50763,0.49976
    ])
    # fmt: on
    if piflav == "lep":
        return np.interp(x, decm, rxsmu)
    else:
        return np.interp(x, decm, rxshm)


def N_data_for(dfs_data, cuts):
    cut = join_cuts(cuts)
    n = []
    for df in dfs_data.values():
        if cut is not None:
            df = df.query(cut)
        n.append(len(df))
    return np.array(n)


def fit_and_plot_breit_wigner(xsec, xsec_err, spe, piflav="lep", ax=None, **kwargs):
    p0 = [91, 2.5, 2]
    f = lambda *args: breit_wigner(*args, piflav=piflav, **kwargs)
    if piflav == "had":
        p0[2] = 20
    pfit, pcov = curve_fit(f, ECM, xsec, sigma=xsec_err, p0=p0, absolute_sigma=True)
    errs = np.sqrt(np.diag(pcov))
    x = np.linspace(87.5, 95, 100)
    ax = ax or plt.gca()
    chi2 = np.sum(((xsec - f(ECM, *pfit)) / xsec_err) ** 2)
    ax.plot(x, f(x, *pfit))
    ax.errorbar(ECM, xsec, xsec_err, fmt=".", color=color[spe])
    ax.text(
        0.67,
        0.65,
        (
            rf"$M_\mathrm{{Z}} = {pfit[0]:.3f}\pm{errs[0]:.3f}$" + "\n"
            rf"$\Gamma = {pfit[1]:.3f}\pm{errs[1]:.3f}$" + "\n"
            rf"$\sigma = {pfit[2]:.3f}\pm{errs[2]:.3f}$" + "\n\n"
            rf"$\chi^2 / \mathrm{{ndf}} = {chi2:.2f} / {len(ECM) - 3}$"
        ),
        transform=ax.transAxes,
    )
    ax.set_xlabel("Center of mass energy [GeV]")
    ax.set_ylabel("Cross section [nb]")

    return ax, pfit, pcov


def combined_breit_wigner_fit(xsecs_and_errs):
    def cost(m, gamma, peak_xsec, rhad):
        y_lep = breit_wigner(ECM, m, gamma, peak_xsec / rhad)
        y_had = breit_wigner(ECM, m, gamma, peak_xsec, piflav="had")

        def get_chi2(y, xsec, xsec_error):
            return (((xsec - y) / xsec_error) ** 2).sum()

        chi2 = (
            get_chi2(y_lep, *xsecs_and_errs["electrons"])
            + get_chi2(y_lep, *xsecs_and_errs["muons"])
            + get_chi2(y_lep, *xsecs_and_errs["taus"])
        )
        chi2 += get_chi2(y_had, *xsecs_and_errs["hadrons"])

        return chi2

    m = Minuit(cost, m=91, gamma=2, peak_xsec=40, rhad=20)
    m.migrad()
    m.hesse()
    return m, cost(*m.values)


def parab(x, par):
    "parabola forumula for AFB fit"
    norm, afb = par
    ergebnis = norm * (1 + x * x) + norm * (4.0 / 3.0) * afb * x
    return ergebnis


def afb_nll(centers, h_data, h_data_err):
    """
    Negative log likelihood to minimize for the fit
    Note: ignores the data errors (assumes poisson probability for the template)
    """

    def cost(pfit):
        # negative log likelihood
        f = parab(centers, pfit)
        y = h_data
        # formula taken from explanation in ROOT TH1::Fit
        # https://root.cern/doc/master/classTH1.html#a7e7d34c91d5ebab4fc9bba3ca47dabdd
        # twice this value can be interpreted like chi2
        # epsilon = 1e-232
        epsilon = 1e-100
        # print('cost, pfit:', pfit)
        return np.sum(f + y * np.log(y / f + epsilon) - y)

    pfit = (h_data.mean(), 0.1)
    m = Minuit(cost, pfit)
    m.errordef = Minuit.LIKELIHOOD
    m.migrad()
    m.hesse()

    param = m.values
    error = m.errors
    debug = m
    return param, error, debug


def histo_costh(df, range=(-0.9, 0.9), bins=51):
    return np.histogram(df.eval("Cos_thru").to_numpy(), range=range, bins=bins)


def afbfit_and_plot(df_mc, df_data, cuts, ratio, centers, ax=None):
    """
    Perform a fit of the forward-backward asymmetry for muons and plot the result.

    Args:
        df_mc (:class:`pandas.DataFrame`): Muon MC events.
        df_data (:class:`pandas.DataFrame`): Data events.
        cuts: (str or list(str)): Selection to apply.
        ratio: (:class:numpy.ndarray`): Histogram containing the efficiency correction.
        ax: The :class:`matplotlib.axes.Axes` to plot on. If not given the current axes is used.

    Returns:
        tuple(float, float, :class:`iminuit.Minuit`): The fitted asymmetry,
            the uncertainty and a an :class:`iminuit.Minuit` object containing information on the fit
        x
    """

    # h_tot, edges = histo_costh(df_mc)
    # h_sel, edges = histo_costh(df_mc.query(cuts))
    # centers = edges[:-1] + 0.5 * (edges[1:] - edges[:-1])
    # corr = h_tot / h_sel

    # correct should be
    # corr = h_sel / h_tot

    corr = ratio

    h_data, edges = histo_costh(df_data.query(join_cuts(cuts)))
    h_data_1 = np.where(h_data >= 1, h_data, 1)
    h_data_err_rel = 1.0 / np.sqrt(h_data_1)
    h_data = h_data * corr
    h_data_err = h_data_err_rel * h_data

    param, error, debug = afb_nll(centers, h_data, h_data_err)

    ax = ax or plt.gca()
    ax.errorbar(centers, h_data, h_data_err, fmt=".", color="black", label="Data")
    ax.plot(centers, parab(centers, param), color="red", linewidth=2, label="Fit")
    ax.set_xlabel("cos(Thrust)")

    afb, dafb = param[1], error[1]

    return afb, dafb, debug


# fmt: off
def qedcor2(de) : 
  
    npo = 60 

    decm = np.array([-5.18840,-4.98840,-4.78840,-4.58840,-4.38840,-4.18840,-3.98840,-3.78840,-3.58840,-3.38840,
    -3.18840,-2.98840,-2.78840,-2.58840,-2.38840,-2.18840,-1.98840,-1.78840,-1.58840,-1.38840,
    -1.18840,-0.98840,-0.78840,-0.58840,-0.38840,-0.18840,0.01160,0.21160,0.41160,0.61160,
    0.81160,1.01160,1.21160,1.41160,1.61160,1.81160,2.01160,2.21160,2.41160,2.61160,2.81160,
    3.01160,3.21160,3.41160,3.61160,3.81160,4.01160,4.21160,4.41160,4.61160,4.81160,
    5.01160,5.21160,5.41160,5.61160,5.81160,6.01160,6.21160,6.41160,6.61160])

    dasmu = np.array([0.0086, 0.0107, 0.0125, 0.0142, 0.0157, 0.0170, 0.0180, 0.0189, 0.0197, 0.0202, 0.0205,
    0.0207, 0.0207, 0.0206, 0.0203, 0.0199, 0.0194, 0.0188, 0.0181, 0.0174, 0.0168, 0.0162,
    0.0158, 0.0156, 0.0158, 0.0165, 0.0178, 0.0199, 0.0227, 0.0264, 0.0309, 0.0363, 0.0423, 
    0.0490, 0.0562, 0.0640, 0.0721, 0.0806, 0.0893, 0.0983, 0.1074, 0.1166, 0.1259, 0.1353,
    0.1447, 0.1541, 0.1634, 0.1727, 0.1819, 0.1910, 0.2000, 0.2088, 0.2176, 0.2262, 0.2346,
    0.2429, 0.2510, 0.2589, 0.2666, 0.2742])
 
    return np.interp(de, decm, dasmu)
# fmt: on


def fitasy(ecm, par, zmass=91.1884):
    "approx formula for asymmerty as function of ecm"
    par0, par1 = par

    result = par0 + par1 * (ecm - zmass)

    if qedcor:
        result -= qedcor2(ecm - zmass)

    return result


def afb_fit(ecm, afb, dafb, qed=True, zmass=91.1884):
    """
    chi2 fit of asymmetry vs ecm
    qedcor: switch to enable qed-corrections
    """

    global qedcor
    qedcor = qed

    def chi2(pfit):
        # negative log likelihood
        f = fitasy(ecm, pfit, zmass)
        y = afb
        return np.sum((y - f) ** 2 / dafb**2)

    pfit = (0.0, 0.1)
    m = Minuit(chi2, pfit)
    m.errordef = Minuit.LIKELIHOOD
    m.migrad()
    m.hesse()

    return m


def load_data():
    if not os.path.exists(HDF_NAME):
        response = requests.get(HDF_URL)
        with open(HDF_NAME, "wb") as f:
            f.write(response.content)

    df_lumi = pd.read_hdf(HDF_NAME, key="Lumi")

    mckeys = ["hadrons", "electrons", "muons", "taus"]

    # TODO: change the hdf file
    en_to_de = {
        "hadrons": "Hadronen",
        "electrons": "Elektronen",
        "muons": "Muonen",
        "taus": "Taus",
    }

    df_mc = {}
    for k in mckeys:
        df_spe = pd.read_hdf(HDF_NAME, key=en_to_de[k])
        df_spe["species"] = k
        df_spe["lumiwgt"] = float(df_lumi["ecm4"]) / float(df_lumi[en_to_de[k]])
        df_mc.update({k: df_spe})

    datakeys = ["ecm" + str(i) for i in range(1, 8)]

    df_data = {}
    for k in datakeys:
        df_ecm = pd.read_hdf(HDF_NAME, key=k)
        df_ecm["species"] = k
        df_data.update({k: df_ecm})

    return df_mc, df_data, df_lumi
