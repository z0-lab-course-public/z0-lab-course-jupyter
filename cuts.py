# Define your cuts here
# Don't forget to save this file after each modification and run:
# %run cuts.py
# in the notebook!

cuts_e = [
    "Ncharged>1",
    "abs(Cos_thru)<0.9",
]

# Muons
cuts_m = [
    "Ncharged>1",
    "abs(Cos_thru)<0.9",
]

# Taus
cuts_t = [
    "Ncharged>1",
    "abs(Cos_thru)<0.9",
]

# Hadrons
cuts_h = [
    #
]

cuts = {
    "hadrons": cuts_h,
    "electrons": cuts_e,
    "muons": cuts_m,
    "taus": cuts_t,
}
