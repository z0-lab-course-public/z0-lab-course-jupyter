from pathlib import Path
import numpy as np

HDF_NAME = "ntz0.h5"
HDF_URL = "http://www.etp.physik.uni-muenchen.de/kurs/Z0-Versuch/data/ntz0.h5"

# center of mass energies for the available data files
ECM = np.array([88.396, 89.376, 90.234, 91.238, 92.068, 93.080, 93.912])

ECM_AFB = np.array([89.45, 91.18, 93.00])

# corresponding luminosities
LUMI = np.array([1303.1, 1394.2, 1265.2, 7901.2, 1321.2, 1356.1, 1493.2])

color = {
    "hadrons": "C3",
    "electrons": "C0",
    "muons": "C2",
    "taus": "C1",
}

# cross sections at z-mass (rough numbers)
PEAK_XSEC = 1.45
RHAD = 20.7
XSEC = {
    "electrons": PEAK_XSEC * 3,
    "muons": PEAK_XSEC,
    "taus": PEAK_XSEC,
    "hadrons": PEAK_XSEC * RHAD,
}
